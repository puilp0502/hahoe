from django.http import HttpResponse
from pprint import pprint

import requests

def clean_header(header):
    # clean hop-by-hop headers
    header.pop('Content-Length', None)
    header.pop('Connection', None)
    header.pop('Keep-Alive', None)
    header.pop('Proxy-Authenticate', None)
    header.pop('Proxy-Authorization', None)
    header.pop('Te', None)
    header.pop('Trailers', None)
    header.pop('Transfer-Encoding', None)
    header.pop('Upgrade', None)
    header.pop('Public', None)


def proxy(request):
    try:
        url = request.GET['url']
    except KeyError:
        return HttpResponse('URL not provided')

    headers = {}
    for k, v in request.META.items():
        if 'HTTP_' in k:
            split_key = k[5:].split('_')
            field = '-'.join(map(lambda w: w[0] + w[1:].lower(), split_key))
            headers[field] = v
    headers['Referer'] = 'http://gall.dcinside.com/'
    try:
        # Redact sensitive or meaningless(hop-by-hop) headers
        clean_header(headers)
        del headers['Host']
        del headers['Cookie']

    except KeyError:
        pass

    recv_resp = requests.get(url=url, headers=headers, stream=True)
    resp = HttpResponse()
    try:
        clean_header(recv_resp.headers)
        del recv_resp.headers['Set-Cookie']
    except KeyError:
        pass

    # Set response headers
    for k, v in recv_resp.headers.items():
        resp[k] = v
    print(resp)
    resp.write(recv_resp.raw.read())
    return resp
